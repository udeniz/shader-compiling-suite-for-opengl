#include <iostream>
#include <fstream>
#include <string>
#include "shader.h"
#include "GL/glew.h"
#include "windows.h"
using namespace std;
shader::shader(){}; // empty default csontructor
shader::shader(const char* vsFile,const char* fsFile)  //default constructor
{
    sinit(vsFile,fsFile);
}
shader::~shader()
{
    glDetachShader(nProgramID,vsident);
    glDetachShader(nProgramID,fsident);
    glDeleteShader(vsident);
    glDeleteShader(fsident);
    glDeleteProgram(nProgramID);
}
void shader::bind() {
    glUseProgram(nProgramID);
}

void shader::unbind() {
    glUseProgram(0);
}
int shader::sinit(const char *vsFile, const char *fsFile)
{
    vsident=glCreateShader(GL_VERTEX_SHADER);  //vertex shader identifier
    fsident=glCreateShader(GL_FRAGMENT_SHADER); // vertex shader identifier
    const GLchar *vsData=ShaderFileReader(vsFile);  // vertex shader data pointer
    const GLchar *fsData=ShaderFileReader(fsFile); // fragment shader data pointer
    glShaderSource(vsident,1,&vsData,0); //Assign vertex source code via identifier
    glShaderSource(fsident,1,&fsData,0);// assign vertex souce code via identifier
    glCompileShader(vsident); //Compile vertex shader source code
    ShaderErrorCheck(vsident,vsFile);  // check vertex shader for errors
    glCompileShader(fsident);  // compile fragment shader source code
    ShaderErrorCheck(fsident,fsFile); // check fragment shader for errors
    glAttachShader(nProgramID,vsident);
    glAttachShader(nProgramID,fsident);
    glLinkProgram(nProgramID);
    ProgramErrorCheck(nProgramID,"failed program");
}
char *shader::ShaderFileReader(const char*sfName)
{
    ifstream sfile(sfName); //input file stream class generator
    sfile.seekg(0,ios::end); // move to the end of file
    int nFileSize=sfile.tellg() ; // value holding the size of shaderfile assigned properly
    char *code = new char[nFileSize+1];  //dynamic memory for our shadersource code +1 for null termination
    sfile.read(code,nFileSize); // read nFileSize amount of char data from our file to our buffer
    code[nFileSize+1]='0'; //ensure null termination of the last char
   return code;  // return our buffer containing shadercode
}
int shader::ShaderErrorCheck(int nShaderIdentifier,const char* filename)
{
    const unsigned int nBufferSize=1024; //buffersize for our error string
    char error[nBufferSize]; //set array for error string
    memset(error,0,nBufferSize);// assign all members of the array to 0
    int nErrorSize=0; // Default Error size(0 means no error)
    glGetShaderInfoLog(nShaderIdentifier,nBufferSize,&nErrorSize,error);
    if (error>0)
    {
        cerr << "Shader compile failure FILENAME:"<< filename <<"error string:"
        <<error<< endl;
    }
}
int shader::ProgramErrorCheck(int nProgramNumber,const char*filename)
{
        const unsigned int nBufferSize=1024; //buffersize for our error string
    char error[nBufferSize]; //set array for error string
    memset(error,0,nBufferSize);// assign all members of the array to 0
    int nErrorSize=0; // Default Error size(0 means no error)
    glGetProgramInfoLog(nProgramID,nBufferSize,&nErrorSize,error);
    if (error>0)
    {
        cerr << "Shader(PROGRAM) link failure"<< filename <<"error string:"
        <<error<< endl;
    }
glValidateProgram(nProgramNumber);
int status;
glGetProgramiv(nProgramNumber,GL_VALIDATE_STATUS,&status);
if(status==GL_FALSE)
{
    cerr<< "Validation Failed For some Reason :("<< endl;
}
}

int shader::id()
{
    return nProgramID;
}
