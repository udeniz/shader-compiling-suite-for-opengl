#ifndef SHADER_H
#define SHADER_H
class shader
{
    private:
    int nProgramID;
    int vsident;
    int fsident;
    char *fsCode;
    shader();
    public:
    ~shader();
    shader(const char* vsFile, const char* fsFile);
    int sinit(const char* vsFile, const char* fsFile);
    char* ShaderFileReader(const char* sfName);
    int ShaderErrorCheck(int shadernumber,const char*filename);
    int ProgramErrorCheck(int nProgramNumber,const char*filename);
    void bind();
    void unbind();
    int id();

};
#endif
